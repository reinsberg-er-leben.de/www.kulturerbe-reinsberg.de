import glob
import os
import sys
import time
import re
from urllib.parse import unquote
 

if os.path.exists('convert.log'):
    os.remove('convert.log')

lf = open('convert.log','a+')

POOL=[]

def log(comment):
    global lf 
    lf.write(comment+'\n') 
    print(comment)

def get_files(d):
    D = glob.glob(d)
    for d in D:
        if os.path.isdir(d):
            get_files(d+'/*')
        else:
            POOL.append(d)


get_files('www.kulturerbe-reinsberg.de/*')

for f in POOL:
    opt = f.split('?')
    if len(opt) == 2:
        # dynamische Files konvertieren
        x = re.search("^w=.*$", opt[1] ) 
        if x != None:
            old_fn = f
            new_fn = '.'.join( opt[0].split('.')[0:-1] ) + '_' + opt[1].replace('=','').replace('&','_') + '.' + opt[0].split('.')[-1]
            log( 'rename file: ' + old_fn + ' => ' + new_fn )
            os.rename( old_fn, new_fn )
            continue
        
        x = re.search("^f=.*$", opt[1] ) 
        if x != None:
            old_fn = f
            new_fn = opt[0].replace('index.html','') + opt[1].replace('f=','').replace('=','').replace('&','_')+'.svg'
            log( 'rename file: ' + old_fn + ' => ' + new_fn )
            os.rename( old_fn, new_fn )
            continue 

        x = re.search("^ca=.*$", opt[1] ) 
        if x != None:
            old_fn = f
            new_fn = opt[0].replace('index.html','') + opt[1].replace('f=','').replace('=','').replace('&','_')+'.svg'
            log( 'rename file: ' + old_fn + ' => ' + new_fn )
            os.rename( old_fn, new_fn )
            continue

        x = re.search("^u=.*$", opt[1] ) 
        if x != None:
            old_fn = f
            new_fn = opt[0].replace('index.html','') + opt[1].replace('=','').replace('&','_')+'.svg'
            log( 'rename file: ' + old_fn + ' => ' + new_fn )
            os.rename( old_fn, new_fn )
            continue

POOL=[]
get_files('www.kulturerbe-reinsberg.de/*')   

# html verlinkungen konvertieren
for f in POOL:
    x = re.search("(.html|.css)", f )
    if x != None:
        log( '\nopen file: ' + f)
        ff = open( f, 'r' )
        code = ff.read()
        ff.close()
        
        X = re.findall("(src=\".*?\"|href=\".*?\"|url\(.*?\)|data-src=\".*?\"|data-src-hd=\".*?\")", code )
        Y = re.findall("srcset=\".*?\"", code )
        for y in Y:
            A = y.replace('srcset="','').replace('"','').replace(' 1x,','').replace(' 2x','').split(' ')
            B = A[0].split('?')
            if len(B) == 2:
                X.append(A[0])
            B = A[1].split('?')
            if len(B) == 2:
                X.append(A[1])

        for x in X:

            if re.search('"http',x) != None:
                continue

            opt = x.split('?')

            if len(opt) == 2:
                
                y = re.search("^w=.*$", opt[1] ) 
                if y != None:
                    old_fn = x.replace('"','').replace("'",'').replace('url(','').replace('data-src-hd=','').replace('data-src=','').replace('src=','').replace('href=','').replace(')','')
                    opt=old_fn.split('?')
                    new_fn = '.'.join( opt[0].split('.')[0:-1] ) + '_' + opt[1].replace('=','').replace('&','_').replace('amp;','') + '.' + opt[0].split('.')[-1]
                    log( 'convert link: ' + old_fn + ' => ' + new_fn )
                    if not os.path.exists( unquote( '/'.join( f.split('/')[0:-1] ) + '/' + new_fn ) ):
                        log( 'file_not_exists: ' + '/'.join( f.split('/')[0:-1] ) + '/' + new_fn )
                        log( 'file_not_exists_cmd: ' + 'wget "https://www.kulturerbe-reinsberg.de/' + old_fn + '" -O "' + '/'.join( f.split('/')[0:-1] ) + '/' + unquote(new_fn) + '"')
                        os.system('wget "https://www.kulturerbe-reinsberg.de/' + old_fn + '" -O "' + '/'.join( f.split('/')[0:-1] ) + '/' +  unquote(new_fn) + '"' )
                    code = code.replace( old_fn, new_fn )
                    continue 

                y = re.search("^f=.*$", opt[1] ) 
                if y != None:
                    old_fn = x.replace('"','').replace("'",'').replace('url(','').replace('src=','').replace('href=','').replace(')','')
                    opt=old_fn.split('?')
                    new_fn = opt[0].replace('index.html','') + opt[1].replace('f=','').replace('=','').replace('&','_').replace('amp;','') + '.svg'
                    log( 'convert link: ' + old_fn + ' => ' + new_fn )
                    if not os.path.exists( unquote( '/'.join( f.split('/')[0:-1] ) + '/' + new_fn ) ):
                        log( 'file_not_exists: ' + '/'.join( f.split('/')[0:-1] ) + '/' + new_fn )
                        os.system('wget "https://www.kulturerbe-reinsberg.dee/' + old_fn + '" -O "' + '/'.join( f.split('/')[0:-1] ) + '/' +  unquote(new_fn) + '"' )
                    code = code.replace( old_fn, new_fn )
                    continue

                y = re.search("^ca=.*$", opt[1] ) 
                if y != None:
                    old_fn = x.replace('"','').replace("'",'').replace('url(','').replace('src=','').replace('href=','').replace(')','')
                    opt=old_fn.split('?')
                    new_fn = opt[0].replace('index.html','') + opt[1].replace('f=','').replace('=','').replace('&','_').replace('amp;','') + '.svg'
                    log( 'convert link: ' + old_fn + ' => ' + new_fn )
                    if not os.path.exists( unquote( '/'.join( f.split('/')[0:-1] ) + '/' + new_fn ) ):
                        log( 'file_not_exists: ' + '/'.join( f.split('/')[0:-1] ) + '/' + new_fn )
                        os.system('wget "https://www.kulturerbe-reinsberg.de/' + old_fn + '" -O "' + '/'.join( f.split('/')[0:-1] ) + '/' +  unquote(new_fn) + '"' )
                    code = code.replace( old_fn, new_fn )
                    continue 

                y = re.search("^u=.*$", opt[1] ) 
                if y != None:
                    old_fn = x.replace('"','').replace("'",'').replace('url(','').replace('src=','').replace('href=','').replace(')','')
                    opt=old_fn.split('?')
                    new_fn = opt[0].replace('index.html','') + opt[1].replace('=','').replace('=','').replace('&','_').replace('amp;','') + '.svg'
                    log( 'convert link: ' + old_fn + ' => ' + new_fn )
                    if not os.path.exists( unquote( '/'.join( f.split('/')[0:-1] ) + '/' + new_fn ) ):
                        log( 'file_not_exists: ' + '/'.join( f.split('/')[0:-1] ) + '/' + new_fn )
                        os.system('wget "https://www.kulturerbe-reinsberg.de/' + old_fn + '" -O "' + '/'.join( f.split('/')[0:-1] ) + '/' +  unquote(new_fn) + '"' )
                    code = code.replace( old_fn, new_fn )
                    continue 

            else:
                if opt[0][-6:] == '/css/"':
                    old_fn = opt[0]
                    new_fn = opt[0][0:-1]+'index.css'+opt[0][-1]
                    log( 'convert link: ' + old_fn + ' => ' + new_fn )
                    code = code.replace( old_fn, new_fn )
                    continue
                
                new_fn = opt[0].replace('"','').replace("'",'').replace('url(','').replace('data-src-hd=','').replace('data-src=','').replace('src=','').replace('href=','').replace(')','')
                
                if new_fn != '#' and new_fn[0:7] != 'mailto:' and new_fn[0:4] != 'tel:' and not os.path.exists( unquote( '/'.join( f.split('/')[0:-1] ) + '/' + new_fn ) ):
                    log( 'new_fn: ' + new_fn )
                    log( 'file_not_exists: ' + '/'.join( f.split('/')[0:-1] ) + '/' + new_fn )
                    log( 'file_not_exists_cmd: ' + 'wget "https://' + '/'.join( f.split('/')[0:-1] ) + '/' + new_fn + '" -O "' + '/'.join( f.split('/')[0:-1] ) + '/' + unquote(new_fn) + '"')
                    os.system('wget "https://' + '/'.join( f.split('/')[0:-1] ) + '/' + new_fn + '" -O "' + '/'.join( f.split('/')[0:-1] ) + '/' +  unquote(new_fn) + '"' )

        log( 'update file: ' + f + '\n' )      
        ff = open( f, 'w' )
        ff.write(code)
        ff.close()

        # shortlinks konvertieren
        x = re.search("/css/index.html", f )
        if x != None:   
            old_fn = f
            new_fn = f.replace('index.html','index.css')
            log( 'rename file: ' + old_fn + ' => ' + new_fn )
            os.rename( old_fn, new_fn )
            continue 

        x = re.search("/js/index.html", f )
        if x != None:
            old_fn = f
            new_fn = f.replace('index.html','index.js')
            log( 'rename file: ' + old_fn + ' => ' + new_fn )
            os.rename( old_fn, new_fn )
            continue

lf.close()