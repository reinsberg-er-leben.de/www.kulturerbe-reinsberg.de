(function($) {
    "use strict";

    var fader,
        lightbox,
        lightboxImage,
        lightboxElement,
        lightboxCaption,
        lightboxIframe,
        closer,
        closerIframe,
        lightboxmode,
        elementWidth,
        elementHeight,
        autoplay,
        lightboxTitle = '',
        nextitem,
        previtem,
        groupid = '',
        alllinksingroup;

    function prepare() {
        if (fader === undefined) {
            fader = $('<div class="fader"></div>');
            lightbox = $('<div class="lightbox"></div>');
            $("body").append(fader);
            $("body").append(lightbox);
            fader.click(hideLightBox);
        }

        nextitem = undefined;
        previtem = undefined;

    }

    $.fn.ngGallery = function() {
        $(this).click(function() {
            if ($(this).data('nolightbox') === true) return false;
            prepare();

            lightboxImage = $('<img />');
            lightboxCaption = $('<em></em>');
            lightboxTitle = $(this).attr('title');
            closer = $('<div class="closer"></div>');

            lightboxmode = 'img';
            fader.fadeTo(200, 0.9);
            lightbox.children().remove();

            lightbox.append(lightboxImage);

            lightboxCaption.html(lightboxTitle);
            lightbox.append(lightboxCaption);

            lightbox.append(closer);

            var group = $(this).attr('data-nggroup');

            if (group !== undefined) {
                alllinksingroup = $('a[data-nggroup="' + group + '"]');
                if (alllinksingroup.length > 1) {
                    nextitem = $('<div class="nextitem"></div>');
                    previtem = $('<div class="previtem"></div>');
                    lightbox.append(nextitem);
                    lightbox.append(previtem);
                    nextitem.click(showNextItem);
                    previtem.click(showPrevItem);
                }
            }

            lightboxImage.click(hideLightBox);
            closer.click(hideLightBox);


            $(window).on('keydown', handleKeyboard);
            $(window).on('resize', positionLightBox);

            var image = new Image();
            $(image).load(function() {
                lightboxImage.attr('src', image.src);
                lightboxImage.data('width', image.width);
                lightboxImage.data('height', image.height);
                positionLightBox();
                lightbox.css({
                    'transition': 'none',
                    'opacity': 0,
                    'transform': 'scale(0.9)',
                    'display': 'block'
                });
                lightbox.height();
                lightbox.css({
                    'transition': 'opacity 0.2s, transform 0.2s',
                    'opacity': 1,
                    'transform': 'none'
                });
            });

            image.src = $(this).attr('href');

            return false;
        });
    };

    $.fn.ngGalleryIframe = function() {
        $(this).click(function() {
            if ($(this).data('nolightbox') === true) return false;
            prepare();

            lightboxIframe = $('<iframe frameborder="0"></iframe>');
            closerIframe = $('<div class="closeriframe"></div>');

            lightboxmode = 'iframe';
            fader.fadeTo(200, 0.9);
            lightboxIframe.attr('src', $(this).attr('href'));

            var width = $(this).attr('data-width');
            if (width === undefined) width = '900';
            var height = $(this).attr('data-height');
            if (height === undefined) height = '700';

            lightboxIframe.data('width', width);
            lightboxIframe.data('height', height);
            lightbox.children().remove();
            lightbox.append(lightboxIframe);
            lightbox.append(closerIframe);
            closerIframe.click(hideLightBox);

            positionLightBox();

            $(window).on('keydown', handleKeyboard);
            $(window).on('resize', positionLightBox);

            return false;
        });
    };

    $.fn.ngGalleryElement = function() {
        $(this).click(function() {
            if ($(this).data('nolightbox') === true) return false;
            prepare();


            lightboxmode = 'element';
            fader.fadeTo(200, 0.9);
            lightboxElement = $(this).attr('data-element');
            elementWidth = parseInt($(this).attr('data-width'));
            elementHeight = parseInt($(this).attr('data-height'));
            autoplay = ($(this).attr('data-autoplay') == 'on');
            closer = $('<div class="closer"></div>');
            closer.click(hideLightBox);

            lightbox.children().remove();
            lightbox.html(lightboxElement);
            lightbox.append(closer);
            positionLightBox();

            $(window).on('keydown', handleKeyboard);
            $(window).on('resize', positionLightBox);

            return false;
        });
    };

    function showNextItem() {
        var current = lastCurrentItemIndex();
        if (current != -1) {
            current++;
            if (current > alllinksingroup.length - 1) current = 0;
            showItem(alllinksingroup[current], 20);
        }
    }

    function showPrevItem() {
        var current = currentItemIndex();
        if (current != -1) {
            current--;
            if (current < 0) current = alllinksingroup.length - 1;
            showItem(alllinksingroup[current], -20);
        }
    }

    function currentItemIndex() {
        for (var i = 0; i < alllinksingroup.length; i++) {
            if (alllinksingroup[i].href == lightboxImage.attr('src')) return i;
        }
        return -1;
    }

    function lastCurrentItemIndex() {
        for (var i = alllinksingroup.length - 1; i >= 0; i--) {
            if (alllinksingroup[i].href == lightboxImage.attr('src')) return i;
        }
        return -1;
    }


    function showItem(link, shift) {
        var image = new Image();
        $(image).load(function() {
            lightboxImage.attr('src', image.src);
            lightboxImage.data('width', image.width);
            lightboxImage.data('height', image.height);
            lightbox.css({
                'transition': 'none',
                'opacity': 0,
                'transform': 'translateX(' + shift + 'px)'
            });

            lightboxTitle = $(link).attr('title');
            if ((lightboxTitle !== undefined) && (lightboxTitle !== '')) {
                lightboxCaption.html(lightboxTitle);
            } else {
                lightboxCaption.html('');
            }


            positionLightBox();
            lightbox.height();
            lightbox.css({
                'transition': 'opacity 0.4s, transform 0.4s',
                'opacity': 1,
                'transform': 'none'
            });
            lightboxCaption.css({
                'transition': 'opacity 0.2s',
                'opacity': 1,
                'display': 'block'
            });
        });

        image.src = link.href;


    }

    function handleKeyboard(e) {
        if (e.keyCode == 27) hideLightBox();

        if (nextitem !== undefined) {
            if (e.keyCode == 39) showNextItem();
            if (e.keyCode == 37) showPrevItem();
        }
    }

    function positionLightBox() {
        switch (lightboxmode) {
            case 'img':
                positionLightBoxImage();
                break;
            case 'iframe':
                positionLightBoxIFrame();
                break;
            case 'element':
                positionLightBoxElement();
                break;
        }
    }

    function positionLightBoxImage() {

        var lbWidth = $(lightboxImage).data('width'),
            lbHeight = $(lightboxImage).data('height'),
            lbRatio = lbWidth / lbHeight,
            offset = 16;

        if (lightboxTitle !== undefined && lightboxTitle !== '') offset = 42;

        if (lbWidth > $(window).width() - 64) {
            lbWidth = Math.floor($(window).width() - 64);
            lbHeight = Math.floor(lbWidth / lbRatio);
        }
        if (lbHeight > $(window).height() - 96) {
            lbHeight = Math.floor($(window).height() - 96);
            lbWidth = Math.floor(lbHeight * lbRatio);
        }

        var lbLeft = Math.floor(($(window).width() - lbWidth - 16) / 2),
            lbTop = Math.floor(($(window).height() - lbHeight - offset) / 2);

        lightbox.css({
            'width': lbWidth + 16,
            'height': lbHeight + offset,
            'left': lbLeft,
            'top': lbTop
        });
        lightboxImage.css({
            'width': lbWidth,
            'height': lbHeight
        });

        if (nextitem !== undefined) {
            var half = Math.floor(lbWidth / 2);
            nextitem.css('width', half);
            previtem.css('width', half);
        }
    }

    function positionLightBoxElement() {
        var lbRatio = elementWidth / elementHeight,
            lbWidth = Math.floor($(window).width() - 128),
            lbHeight = Math.floor(lbWidth / lbRatio);

        if (lbHeight > $(window).height() - 128) {
            lbHeight = Math.floor($(window).height() - 128);
            lbWidth = Math.floor(lbHeight * lbRatio);
        }

        var lbLeft = Math.floor(($(window).width() - lbWidth) / 2),
            lbTop = Math.floor(($(window).height() - lbHeight) / 2);

        lightbox.css({
            'width': lbWidth,
            'height': lbHeight,
            'left': lbLeft,
            'top': lbTop
        });
        lightbox.children().eq(0).attr({
            width: lbWidth,
            height: lbHeight
        });
        lightbox.show();
    }

    function positionLightBoxIFrame() {
        var lbWidth = lightboxIframe.data('width'),
            lbHeight = lightboxIframe.data('height');

        if (lbWidth > $(window).width() - 68) {
            lbWidth = Math.floor($(window).width() - 68);
        }
        if (lbHeight > $(window).height() - 68) {
            lbHeight = Math.floor($(window).height() - 68);
        }

        var lbLeft = Math.floor(($(window).width() - lbWidth) / 2),
            lbTop = Math.floor(($(window).height() - lbHeight) / 2);

        lightbox.css({
            'width': lbWidth,
            'height': lbHeight,
            'left': lbLeft,
            'top': lbTop
        });

        lightboxIframe.css({
            'width': lbWidth,
            'height': lbHeight
        });


        lightbox.show();
    }

    function hideLightBox() {
        switch (lightboxmode) {
            case 'img':
                lightbox.fadeOut(200);
                fader.fadeOut(200);
                break;
            case 'iframe':
                lightbox.hide();
                fader.hide();
                lightbox.children().remove();
                break;
            case 'element':
                lightbox.hide();
                fader.hide();
                lightbox.children().remove();
                break;
        }

        $(window).off('keydown', handleKeyboard);
        $(window).off('resize', positionLightBox);

        return false;
    }
})(jQuery);

$(document).ready(function() {
    $('a.gallery').ngGallery();
    $('a.galleryiframe').ngGalleryIframe();
    $('a.galleryelement').ngGalleryElement();
});
